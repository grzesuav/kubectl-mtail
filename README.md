# kubectl-mtail

This repository contains a `kubectl plugin` called `mtail`, which tail logs from multiple pods matching label selector.


You can install this plugin on your machine with `krew` plugin manager:
https://github.com/GoogleContainerTools/krew
```
kubectl krew install mtail
```

## Usage
To see all options please use : `kubectl mtail --help`.

Usage: `kubectl mtail [-c|--container <arg>] [-k|--(no-)colored-pods] [-h|--help] <label-selector>`

Additional options added by mtail are:
* `--follow` - it will not quit but actively waiting for new entries
* `--tail=10` - from running pods - only 10 last entries will be display from history (but new one will be displayed) 

As for now it will only display pods from active namespace.